#!/usr/bin/python
import FastaReader
import Species
import webbrowser, os

print("\nBienvenido a tu comparacion de cadenas de ADN rapida y eficas\n")

error_length = "Cantidad de nucleotidos incorrecto, debe ingresar entre de 40 a 60 nucleotidos."
error_caracters = "Nucleotidos incorrectos, la nomenclatura es: A, G, T, C"

#funcion para ingresar las cadenas y hacer su validacion de longitud y caracteres
def inputString(countString):
	valStr = input("Ingresar seceuncia " + str(countString) + ": ")
	strLength = len(valStr)

	if (strLength not in range(1, 60)):
		print(error_length)
		return None
	
	incorrectLetters = False
	for char in valStr:
		if (char != "A" and char != "G" and char != "T" and char != "C"):
			incorrectLetters = True 
	if (incorrectLetters):
		print(error_caracters)
		return None
	else:
		return valStr

#funcion para ingresar opcion Global o Local
def optionGlobalOrLocal():
	print("\n\nSeleccionar una de las siguientes alineaciones:\n\n1. para continuar con la opcion Global\n2. para continuar con la opcion Local\n")
	option = int(input("Ingresar opcion: "))

	if(option != 1 and option != 2):
		print("Opcion incorrecta")
	else:
		return option

#Numero minimo de caracteres a comparar por si los arreglos tienen diferentes longitudes
def minLenthArrayString(array):
	minLengthAllItems = len(array[0])
	for string in array:
		auxLen = len(string)
		if (auxLen < minLengthAllItems):
			minLengthAllItems = auxLen
	return minLengthAllItems


def compareArray(array):
	numArray = len(array)
	minLengthAllItems = minLenthArrayString(array)

	countSameItems = 0
	#comparar las los valores en la posicion i en todas las cadenas
	for i in range(minLengthAllItems):
		sameCharacter = True
		charBefore = array[0][i]
		for j in range(1,numArray):
			if(charBefore != array[j][i]):
				sameCharacter = False
				break

		if(sameCharacter):
			countSameItems += 1

	return countSameItems


def optGlobal(array):
	numArray = len(array)
	if(numArray <= 1):
		return None

	minLengthAllItems = minLenthArrayString(array)

	countSameItems = compareArray(array)

	print("\n\nCoincidencias: " + str(countSameItems) + "/" + str(minLengthAllItems))

	percentage = countSameItems*100/minLengthAllItems
	print("Porcentaje: " + str(percentage) + "%")
	

#funcion que valida el rango con la cadena previamente ingresada, retorna falso en caso tal de que se exceda los limites
def validRangeWithString(valRange, string):
	first, second = valRange
	valMin, valMax = (0, len(string)-1)

	if(first < valMin or second > valMax):
		return False
	else:
		return True


#le pide al usuario el rando de la cadena en un formato especifico
def getRange(count, string):
	errorInput = True
	attempts = 0
	while errorInput:
		if(attempts > 2):
			return
		valRange = input("Ingresar rango de la secuencia " + str(count+1) + ": ")
		if(valRange.count("-") == 1):
			split = valRange.split("-")
			first = split[0] 
			second = split[1]
			if(first.isnumeric() and second.isnumeric()):
				auxRange = (int(first), int(second))
				if(validRangeWithString(auxRange, string)):
					errorInput = False
					pair = auxRange
				else:
					print("Fuera del rango de la secuencia\n")
			else:
				print("Formato incorrecto\n")
		else:
			print("Formato incorrecto\n")
		attempts += 1
	return pair


def getNewArray(ranges, array):
	newArray = []
	if(len(ranges) != len(array)):
		print("Error de tamaños")
		return

	for i in range(len(array)): 
		string = array[i]
		first, second = ranges[i]
		newArray.append(string[first:second+1])

	return newArray


def getRanges(array):
	print("\n\nA continacion se necesitan los rangos para comparar las seceuncias,\npor favor escribir el rango en el formato n-m, ejemplo 0-10\n")
	ranges = []
	for i in range(len(array)):
		rangeStr = getRange(i, array[i])
		if(rangeStr == None):
			return None
		else:
			ranges.append(rangeStr)

	return ranges


def optLocal(array):
	numArray = len(array)
	if(numArray <= 1):
		return None

	ranges = getRanges(array)

	if (ranges != None):
		newArray = getNewArray(ranges, array)

		minLengthAllItems = minLenthArrayString(newArray)

		countSameItems = compareArray(newArray)

		print("\n\nCoincidencias: " + str(countSameItems) + "/" + str(minLengthAllItems))


def main() :
	#cantidad de cadenas a pedir
	numItems = 2
	#arreglo para guardar las cadenas
	array = []
	option = int(input("""¿Qué desea hacer?
	1. Comparar dos cadenas de texto de genomas
	2. Comparar dos archivos .fasta		
	3. Consultar la ficha de alguna especie
	
	Seleccione una opción: """))
	if option == 3:
		pdfpath = os.getcwd()+"/consultapdf"
		pdffiles = FastaReader.get_pdf(pdfpath)
		pdffileslist = str(pdffiles).replace(",", "\n").replace("[", "").replace("]", "")
		species_id = input(f"""Se han encontrado las siguientes fichas de especies:
		
		{pdffileslist}

		Digite el id de la especie cuya ficha desea consultar: """)

		species_was_found = False
		species_pdffile = ""
		for pdffile in pdffiles:
			if pdffile.__contains__(species_id):
				species_was_found = True
				species_pdffile = pdffile
				break
		if species_was_found:
			webbrowser.open(pdfpath+"/"+species_pdffile)
		else:
			print(f"No se encontró ninguna especie con un id = {species_id}")

	else:
		if option == 1:		
			#for para pedir dinamicamente la cantidad de cadenas
			for i in range(numItems):
				strData = inputString(i+1)
				if(strData != None):
					array.append(strData)
				else:
					break		
		elif(option == 2):		
			fasta_files = FastaReader.get_files()		
			first_species = None
			second_species = None		
			for j in range(0, len(fasta_files)):			
				reader = open(fasta_files[j], 'r')
				splitted_content = ''.join(reader.readlines()).split(',')
				species_name, species_genome = splitted_content
				species_genome = species_genome[15:75]
				if j == 0:
					first_species = Species.Species(species_name, species_genome)
				if j == 1:
					second_species = Species.Species(species_name, species_genome)
				array.append(species_genome)			
				reader.close()
			print(f"""Comparación de genomas entre las siguientes especies:
				{first_species.species_name}
				{second_species.species_name}""")

		#validar la cantidad y continuar con el flujo
		if(len(array) == numItems):
			option = optionGlobalOrLocal()
			if(option == 1):
				optGlobal(array)
			elif (option == 2):
				optLocal(array)	
main()
