import sys
from os import listdir
from os.path import isfile, join

def get_files():
    files = []
    # argv[1:] toma los parámetros por línea de comandos que vengan después del nombre del ejecutable python (en este caso los archivos que se van a leer)
    for filename in sys.argv[1:]:
        files.append(filename)
    return files

def get_pdf(path):
    onlyfiles = [f for f in listdir(path) if isfile(join(path, f))]
    return onlyfiles